import { useEffect, useState } from 'react'
import debounce from 'lodash/debounce'
import styled from 'styled-components'
import { Table, Flex, Loader } from 'design-system'
import useEventListener from '@use-it/event-listener'

let webSocket = null
const UPDATE_DELAY = 20
const MAX_DEPTH = 20
const HEARTBEAT_DELAY = 30000

const OrderBook = () => {
  const [bids, setBids] = useState([])
  const [asks, setAsks] = useState([])
  const [maxTotal, setMaxTotal] = useState(0)
  const [readyState, setReadyState] = useState('CONNECTING')
  let heartbeatInterval = null

  let bidsBuffer = new Map()
  let asksBuffer = new Map()

  const updateUI = debounce(() => {
    const bidsToDisplay = Array.from(bidsBuffer.entries())
      .slice(0, MAX_DEPTH)
      .map(bid => ({
        price: bid[0],
        size: bid[1][0],
        total: bid[1][1]
      }))
    const asksToDisplay = Array.from(asksBuffer.entries())
      .reverse()
      .slice(-MAX_DEPTH)
      .map(ask => ({
        price: ask[0],
        size: ask[1][0],
        total: ask[1][1]
      }))
    setBids(bidsToDisplay)
    setAsks(asksToDisplay)

    if (bidsToDisplay && bidsToDisplay.length === MAX_DEPTH) {
      setMaxTotal(
        Math.max(asksToDisplay[0].total, bidsToDisplay[MAX_DEPTH - 1].total)
      )
    }
  }, UPDATE_DELAY)

  const populateBufferFromSnapshots = (orders, type) => {
    let buffer = new Map()
    let prevTotal = 0
    orders
      .sort((a, b) => (type === 'bids' ? b[0] - a[0] : a[0] - b[0]))
      .forEach(order => {
        const [price, size] = order
        const total = size + prevTotal
        buffer.set(price, [size, total])
        prevTotal = total
      })

    return buffer
  }

  const updateBuffer = (orders, buffer, type) => {
    let newBuffer = buffer
    if (orders && orders.length > 0) {
      // iterate on all the bids, remove canceled bids, override updated bids
      orders.forEach(order => {
        const [price, size] = order
        // if bid exist in buffer?
        if (!!newBuffer.get(price) && size === 0) {
          // remove it from buffer
          newBuffer.delete(price)
        } else {
          if (size !== 0) {
            newBuffer.set(price, [size, 0])
          }
        }
      })

      let prevTotal = 0

      // re-sort the map and recalculate totals
      newBuffer = new Map(
        [...newBuffer.entries()]
          .sort((a, b) => (type === 'bids' ? b[0] - a[0] : a[0] - b[0]))
          // keep track of max buffer size to create room for deleted buffers
          .map(order => {
            const [price, [size]] = order
            const total = size + prevTotal
            const newOrder = [price, [size, total]]
            prevTotal = total
            return newOrder
          })
      )
    }

    return newBuffer
  }

  const subscribe = () => {
    if (!webSocket && readyState !== 'OPEN') {
      webSocket = new WebSocket('wss://www.cryptofacilities.com/ws/v1?orderbook')

      webSocket.onopen = event => {
        const subscriptionMsg = {
          event: 'subscribe',
          feed: 'book_ui_1',
          product_ids: ['PI_XBTUSD']
        }

        const heartbeatMsg = {
          event: 'subscribe',
          feed: 'heartbeat'
        }

        webSocket.send(JSON.stringify(subscriptionMsg))

        heartbeatInterval = setInterval(() => {
          if (webSocket && readyState === 'OPEN') {
            webSocket.send(JSON.stringify(heartbeatMsg))
          }
        }, HEARTBEAT_DELAY)

        setReadyState('OPEN')
      }

      webSocket.onclose = event => {
        setReadyState('CLOSED')
      }

      webSocket.onerror = event => {
        webSocket.close()
        webSocket = null
        setReadyState('ERROR')
      }

      webSocket.onmessage = e => {
        const data = JSON.parse(e.data)

        if (data.feed === 'book_ui_1_snapshot') {
          bidsBuffer = populateBufferFromSnapshots(data.bids, 'bids')
          asksBuffer = populateBufferFromSnapshots(data.asks, 'asks')
          updateUI()
        }

        if (data.feed === 'book_ui_1') {
          bidsBuffer = updateBuffer(data.bids, bidsBuffer, 'bids')
          asksBuffer = updateBuffer(data.asks, asksBuffer, 'asks')
          updateUI()
        }
      }
    }
  }

  const unsubscribe = () => {
    if (webSocket) {
      webSocket.close()
    }
    clearInterval(heartbeatInterval)
    webSocket = null
  }

  useEffect(() => {
    if (readyState === 'CONNECTING' || readyState === 'ERROR') {
      subscribe()
    }
  }, [readyState])

  // Unsubscribe after 30 seconds of not being on active tab 
  useEventListener('visibilitychange', () => {
    const isHidden = global.document.visibilityState === 'hidden'
    if (isHidden) {
      setTimeout(() => {
        unsubscribe()
      }, 3000)
    } else {
      setReadyState('CONNECTING')
    }
  }, global.document)

  if (readyState === 'CONNECTING') {
    return (
      <Flex alignItems='center' justifyContent='center' py={5}>
        <Loader />
      </Flex>
    )
  } else {
    return (
      <Flex flexDirection='column'>
        <Table>
          <thead>
            <tr>
              <th>Price (XBT/USD)</th>
              <th>Size</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {asks.map(ask => (
              <tr
                key={ask.price}
                style={{
                  background: `linear-gradient(
                  to right,
                  var(--red-bg) ${(ask.total / maxTotal) * 100}%,
                  transparent ${(ask.total / maxTotal) * 100}%
                )`
                }}
              >
                <td style={{ color: 'var(--red)' }}>{ask.price}</td>
                <td>{ask.size}</td>
                <td>{ask.total}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Table>
          <tbody>
            {bids.map(bid => (
              <tr
                style={{
                  background: `linear-gradient(
                  to right,
                  var(--green-bg) ${(bid.total / maxTotal) * 100}%,
                  transparent ${(bid.total / maxTotal) * 100}%
                )`
                }}
                key={bid.price}
              >
                <td style={{ color: 'var(--green)' }}>{bid.price}</td>
                <td>{bid.size}</td>
                <td>{bid.total}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Flex>
    )
  }
}

export default OrderBook
