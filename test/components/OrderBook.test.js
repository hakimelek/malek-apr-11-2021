import React from 'react'
import { render, fireEvent } from '../testUtils'
import OrderBook from '../../components/OrderBook'

describe('OrderBook components', () => {
  it('matches snapshot', () => {
    const { asFragment } = render(<OrderBook />, {})
    expect(asFragment()).toMatchSnapshot()
  })
})
