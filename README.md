## Orderbook Front-end Interview
Built in next.js and styled-components.

### Getting started

Install dependencies
```
npm install
```

Run the application

```
npm run dev
```

Testing:

```
npm test
```
