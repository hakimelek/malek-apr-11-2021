module.exports = {
  future: {
    webpack5: true,
  },
  async headers() {
    return [
      {
        source: '/',
        headers: [
          {
            key: 'Access-Control-Allow-Origin',
            value: 'cryptofacilities.com',
          }
        ],
      },
    ]
  }
}
