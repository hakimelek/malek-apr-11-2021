import styled from 'styled-components'

const Container = styled.div`
  background: var(--bg);
  max-width: 1200px;
  margin: 0 auto;
  color: var(--primary-text);
  font-family: Helvetica, Arial;
  height: 100%;
  min-height: 100vh;
`

export default Container
