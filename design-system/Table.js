import styled from 'styled-components'

const Table = styled.table`
  padding: 20px;
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  table-layout: fixed;

  td {
    text-align: center;
    font-size: 0.8em;
  }

  th {
    color: #eaeaea;
  }
`

export default Table
