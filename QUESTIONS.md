### Questions:

#### 1. What would you add to your solution if you had more time?

Here are the list of tasks I would keep working on:
- Convert the OrderBook component to Typescript.
- Add more unit tests.
- Add update indicator or updated levels (flash colors or prices for example) as a new feature.
- Abstract websocket logic to its own file.
- Depending on the usage of the OrderBook component, expose configuration props.
- Further potential performance optimizations.

#### 2. What would you have done differently if you knew this page was going to get thousands of views per second vs per week?

I have tried to design this page to be scalable. First, this is application is taking advantage of next.js which takes care of automatically code split the js bundles by page. Although next.js is known for SSR, the OrderBook component is being loaded client side only to avoid the server side rendering overhead that could become an issue for a large number of views. The application is listening to the websocket endpoint but the updates are being debounced within a period of 200ms which help us overload the UI with very quick updates. Rendering also is being optimized using inline css with a little overhead on the browser main thread. 


#### 3. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.

Disconnecting the websocket when users leave the tab and reconnecting when they are back to the page. 

```
  useEventListener('visibilitychange', () => {
    const isHidden = global.document.visibilityState === 'hidden'
    if (isHidden) {
      setTimeout(() => {
        unsubscribe()
      }, 3000)
    } else {
      setReadyState('CONNECTING')
    }
  }, global.document)
```

#### 4. How would you track down a performance issue in production? Have you ever had to do this?

Yes, performance issues can be best debugged using the Performance tab in the browser given that this is a client feature. The tab provides multipe features that shows the amount of time we are spending on partical tasks broken down by js parsing and browser rendering. Ideally, performance will need to be monitoring using real user monitoring on a sampled or non-sampled basis. This will help us perform further analasis on the performance by partical user group (by user agent for example).

#### 5. Can you describe common security concerns to consider for a frontend developer?

Endpoints that the front-end consumes needs to be authenticated. Private keys will need to be protected on the server side and never share on the client. Connections need to be served on HTTPS. To avoid Cross-site request forgery (CSRF), pages will need to be served with unique tokens that are generated on the server and these tokens will be passed with every API request and reverified on the server. In cases where react is used, we need to avoid using unsafe functions like dangerouslySetInnerHTML to avoid Cross Site Scripting (XSS) attacks.

#### 6. How would you improve the Kraken API that you just used?

In the case of serving short list of ordder book levels (say less than 100), I would keep serving the snapshopts instead of individual order book deltas. The snapshots will calculate the totals on the server side. This will reduce the amount of work done on the client calculating the new order books and provide a consistent experience to all the clients. The client will fetch the snapshots and will need to just update the UI.
