import { createGlobalStyle, ThemeProvider } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  body {
    --primary-text: #eaeaea;
    --bg: #252a34;
    --green: #12ab79;
    --green-bg: rgba(18, 171, 121, 0.2);
    --red: #ca3e47;
    --red-bg: rgba(202, 62, 71, 0.2);

    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background: var(--bg);
  }
`

const theme = {}

export default function App({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  )
}
