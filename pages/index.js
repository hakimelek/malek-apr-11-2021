import dynamic from 'next/dynamic'
import { Container, Loader, Flex } from 'design-system'

const OrderBook = dynamic(
  () => import('components/OrderBook'),
  { ssr: false }
)

export default function Home() {
  return (
    <Container>
      <OrderBook />
    </Container>
  )
}
